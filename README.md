## cookiecutter-enb-experiment

This template is for building and sharing experiments using Jupyter Notebooks on Ellip Notebooks.

### Information to be provided at creation:

* **experiment** this is the experiment folder
* **summary** this is the experiment summary
* **description** this is the experiment description
* **folder** this is the experiment folder as well as the software repository
* **environment** this is the name of the conda environment for the experiment (makes it shareable and reproducible)
* **module** this the module prefix for the the experiment helper module
* **version** this is the experiment version
* **git_url** this is the git repository URL. The software repository must be the same as the experiment folder

### Experiment structure

It provides under the root `folder`:

* A notebook `experiment.ipynb`
* A helpers python module for the experiment classes and functions named ``<module>_helpers.py`
* A conda environment file named environment.yml for the `module` environment to ensure the portability of the experiment to other platform users
* A README.md file with information on how to use the experiment
* A pom file to trace the versions and enable ciop-release as a tool to release experiments

### Hooks:

The pre_gen_project hook checks the `module` name provided for Python module validity

### Getting started

Instantiate the template with:

```bash
cookiecutter https://gitlab.com/ellip/cookiecutter/notebook/cookiecutter-enb-experiment.git
```

If asked "You've cloned /home/user/.cookiecutters/cookiecutter-enb-experiment before. Is it okay to delete and re-clone it? [yes]:", type `yes`

