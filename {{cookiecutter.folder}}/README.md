## {{cookiecutter.experiment}}

{{cookiecutter.summary}}

{{cookiecutter.description}}

### Getting the experiment

This experiment is hosted in a software repository.

Use `git` to clone it:

```bash
cd /workspace
git clone {{cookiecutter.git_url}}
cd {{cookiecutter.folder}}
```

### Configuring the Python conda environment

The file `environment.yml` contains the Python conda environment for running the notebooks contained in this folder.

From the shell, run:

```bash
conda env create --file=environment.yml
```

Once the environment configuration is done, you can activate it:

```bash
conda activate {{cookiecutter.environment}}
```

### Running the experiment

Open the `environment.ipynb` notebook and update the kernel to use `{{cookiecutter.environment}}`

Run the experiment by executing each of the cells with `shift` + `Enter`.

If asked for the credentials, provide your Ellip username and associated Ellip API key.

### Improving the experiment in a development branch

This experiment is under version control and uses the git flow method (see [https://datasift.github.io/gitflow/IntroducingGitFlow.html])

If not done previously, clone the experiment repository:

```bash
git clone {{cookiecutter.git_url}}
cd {{cookiecutter.folder}}
```

Then, checkout the `develop` branch with:

```bash
git checkout develop
```

At this stage, update the experiment.

When done:

```bash
git add -A
git commit -m '<commit message>'
git pull
```

Finally, do a release with:

```bash
ciop-release
```





